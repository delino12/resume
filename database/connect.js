require("dotenv").config();
var mongoose = require('mongoose');
var MongoClient = require('mongodb').MongoClient

// check application state
var db_url;

// console.log(process.env.NODE_ENV);
switch(process.env.NODE_ENV){
	case "developement":
		db_url = process.env.DATABASE_URL;
		break;
	case "production":
		db_url = process.env.DATABASE_URL;
		break;
	default: 
		db_url = process.env.DATABASE_URL;
		break;
}

// database connection
mongoose.connect('mongodb://localhost:27017/el_db', { useNewUrlParser: true }).then(conn => console.log(`Database connected to el_db`));

// init promise
mongoose.Promise 	= global.Promise;

// export database configuration
module.exports = mongoose;

