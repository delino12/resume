require("dotenv").config();
// check application state
let db_url;
// console log value
// console.log(process.env.NODE_ENV);
switch(process.env.NODE_ENV){
	case "developement":
		db_url = process.env.DATABASE_URL_LOCAL;
		break;
	case "production":
		db_url = process.env.DATABASE_URL_LOCAL;
		break;
	default: 
		db_url = process.env.DATABASE_URL_LOCAL;
		break;
}

// export database configuration
module.exports = db_url;

