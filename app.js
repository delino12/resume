var express       = require('express');
var path          = require('path');
var favicon       = require('serve-favicon');
var logger        = require('morgan');
var cookieParser  = require('cookie-parser');
var bodyParser    = require('body-parser');
var handlebars    = require('express-handlebars');
var session       = require('express-session');
var redis         = require('redis');
// var redisStore    = require('connect-redis')(session);
// var client        = redis.createClient();

// routes init
var index         = require('./routes/index');
var articles      = require('./routes/articles');
var user          = require('./routes/users');
var auth          = require('./routes/auth');
var api           = require('./routes/api');
var subscription  = require('./routes/subscription');

var app = express();

app.use(express.static(path.join(__dirname, 'public')));
app.engine('handlebars', handlebars({defaultLayout: 'main'}));
app.set('view engine', 'handlebars');

app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));

app.use(session({
  secret: 'delino12',
  // store: new redisStore({ host: 'localhost', port: 6379, client: client,ttl : 260}),
  resave: true,
  saveUninitialized: true,
  cookie: { 
    path: '/',
    _expires: new Date(Date.now() + 3600000 * 12 * 12),
    originalMaxAge: 3600000 * 12 * 12,
    httpOnly: false,
    secure: false
  }
}))

app.use(function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*')
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE')
  next()
});

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

app.use('/', index);
app.use('/articles', articles);
app.use('/user', user);
app.use('/auth', auth);
app.use('/api', api);
app.use('/subscription', subscription);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  if(err.status == 404){
    res.render('404');
  }else if(err.status == 500){
    res.render('500');
  }else{
    next(err);
  }
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
