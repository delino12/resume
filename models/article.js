/**
* Author: Ekpoto Liberty Bernard
* Version: 1.0.2
* Signature: delino12
* Model
*/

// init mongoose
const connection    = require('../database/connect');
const mongoose      = require('mongoose');
const ObjectId      = require('mongodb').ObjectId;
const events        = require('../events/new-article');
const Subscription  = require("../models/subscriptions");

// user schema
const articleSchema = new mongoose.Schema({
    user_id: {type: mongoose.Schema.Types.ObjectId, required: true},
    title: {type: String, required: true},
    contents: {type: String, required: true},
    featured_image: {type: String, required: false},
    status: {type: Number, required: true},
    tags: {type: Array, required: false},
    repository: {type: String, required: false}
},{
	timestamps: true,
	autoIndex: false
});

const Article = connection.model('Article', articleSchema);

Article.createIndexes();

Article.addArticle = async (req, res) => {
    let status;
    let message;
	let data = {}
	let query = {
	  	user_id: ObjectId(req.session.user._id),
		title: req.body.title,
		contents: req.body.contents,
		featured_image: req.body.image,
		status: 0,
        tags: req.body.article_tags
	}

  	await new Article(query).save().then(val => {
  		// console.log(val);
  		status 	    = 200;
  		message 	= "Article was added successfully!";
  		data 		= val;
  	}).catch(err => {
  		console.log(err);
  		status 	    = 500;
  		message 	= "Server error";
  		data 		= JSON.stringify(err);
  	});

    res.status(status).json({status, message, data})
}

Article.updateOneArticle = async (req, res) => {
    // console.log(req.body.article_tags)
    let status;
    let message;
    let data = {}
    let query = {
        title: req.body.title,
        contents: req.body.contents,
        featured_image: req.body.image,
        status: 0,
        tags: req.body.article_tags
    }

    await Article.updateOne({_id: ObjectId(req.body.article_id)}, query).then(val => {
        // console.log(val);
        status       = 200;
        message      = "Article was updated successfully!";
        data         = val;
    }).catch(err => {
        console.log(err);
        status      = 500;
        message     = "Server error";
        data        = err;
    });

    res.status(status).json({status, message, data})
}

Article.getUserArticles = async (req, res) => {
    let status;
    let message;
    let data = {}
	let query = {user_id: ObjectId(req.session.user._id)}

  	await Article.find({}).then(val => {
  		// console.log(val);
  		status 	    = 200;
  		message 	= "Article was retrieved successfully!";
  		data 		= val;
  	}).catch(err => {
  		// console.log(err);
  		status 	    = 500;
  		message 	= "Server error";
  		data 		= err;
  	});

  	// return
  	res.status(status).json({status, message, data});
}

Article.getReleasedArticles = async (req, res) => {
    await Article.find({status: 1}).sort({createdAt: -1}).then(val => {
        // console.log(val);
        status      = 200;
        message     = "Article was retrieved successfully!";
        data        = val;
    }).catch(err => {
        // console.log(err);
        status      = 500;
        message     = "Server error";
        data        = err;
    });

    // return
    res.status(status).json({status, message, data});
}

Article.getOneArticle = async (req, res) => {
    let status;
    let message;
    let data = {}
    let query = {
        _id: ObjectId(req.params.articleId)
    }

    await Article.findOne(query).then(val => {
        // console.log(val);
        status   = 200;
        message  = "Article was retrieved successfully!";
        data     = val;
    }).catch(err => {
        // console.log(err);
        status   = 500;
        message  = "Server error";
        data     = JSON.stringify(err);
    });

    // return
    res.status(status).json({status, message, data})
}

Article.releaseArticle = async (req, res) => {
    // console.log(req.body.article_tags)
    let status;
    let message;
    let data = {}
    let query = {
        status: 1
    }

    await Article.updateOne({_id: ObjectId(req.body.article_id)}, query).then( async (val) => {
        // console.log(val);
        status       = 200;
        message      = "Article has been released to the public successfully!";
        data         = val;

        // notify all subscribed users
        var article = await Article.findOne({_id: ObjectId(req.body.article_id)}).then(article => article);
        await Subscription.find({}).then(async subscribed_emails => {
            for (var i = 0; i < subscribed_emails.length; i++) {
                var email = subscribed_emails[i].email;
                // await
                await events.notifyNewArticle(email, article).then(val => console.log('Mail notifications sent!'));
            }
        });
    }).catch(err => {
        console.log(err);
        status      = 500;
        message     = "Server error";
        data        = JSON.stringify(err);
    });

    res.status(status).json({status, message, data})
}

Article.totalPublished = async (req, res) => {
    let status;
    let message;
    let data = {}
    let query = {user_id: ObjectId(req.session.user._id), status: 1}

    await Article.countDocuments(query).then(val => {
        // console.log(val);
        status      = 200;
        message     = "Total Article was retrieved successfully!";
        data        = val;
    }).catch(err => {
        // console.log(err);
        status      = 500;
        message     = "Server error";
        data        = err;
    });

    // return
    res.status(status).json({status, message, data});
}

Article.totalDraft = async (req, res) => {
    let status;
    let message;
    let data = {}
    let query = {user_id: ObjectId(req.session.user._id), status: 0}

    await Article.countDocuments(query).then(val => {
        // console.log(val);
        status      = 200;
        message     = "Total Article was retrieved successfully!";
        data        = val;
    }).catch(err => {
        // console.log(err);
        status      = 500;
        message     = "Server error";
        data        = err;
    });

    // return
    res.status(status).json({status, message, data});
}

Article.deleteOneArticle = function (req, res) {
    Article.remove({_id: ObjectId(req.body.article_id)}).then(val => {
        status     = 200;
        message    = "Article was deleted successfully!";
        data       = val;
    }).catch(err => {
        // console.log(err);
        status     = 500;
        message    = "Server error";
        data       = JSON.stringify(err);
    })

    // return
    res.status(status).json({status, message, data})
}

module.exports = Article;