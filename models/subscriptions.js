/**
* Author: Ekpoto Liberty Bernard
* Version: 1.0.2
* Signature: delino12
* Model
*/
require('dotenv').config();

// init mongoose
const connection  	= require('../database/connect');
const mongoose    	= require('mongoose');
const ObjectId    	= require('mongodb').ObjectId;

// user schema
const subscriptionSchema = new mongoose.Schema({
    email: {type: String, required: true},
    status: {type: Number, required: true}
},{
    timestamps: true,
    autoIndex: false
});

const Subscription = connection.model('Subscription', subscriptionSchema);

Subscription.createIndexes();

Subscription.subscribe = async (req, res) => {
    var email  = req.body.email;
    var status = 1;
    var query  = {email, status}

    await new Subscription(query).save().then(user => {
        res.status(200).json({status: "success", message: "Registration successful!", data: user});
    }).catch(err => {
        res.status(500).json({status: "error", message: "Server error!",  err: err});
    });
}

Subscription.unsubscribe = async (req, res) => {
	var email  = req.params.email;
    var status = 0;
    var query  = {email, status}

    await new Subscription(query).save().then(user => {
        res.status(200).json({status: "success", message: "Registration successful!", data: user});
    }).catch(err => {
        res.status(500).json({status: "error", message: "Server error!",  err: err});
    });
}

Subscription.subscribedUsers = async (req, res) => {
    var email  = req.params.email;

    await Subscription.find({}).sort({createdAt: -1}).then(users => {
        res.status(200).json({status: "success", message: "retrieve subscribe!", data: users});
    }).catch(err => {
        res.status(500).json({status: "error", message: "Server error!",  err: err});
    });
}

module.exports = Subscription;