/**
* Author: Ekpoto Liberty Bernard
* Version: 1.0.2
* Signature: delino12
* Model
*/
require('dotenv').config();

// init mongoose
const connection  	= require('../database/connect');
const mongoose    	= require('mongoose');
const ObjectId    	= require('mongodb').ObjectId;
const bcrypt      	= require('bcryptjs');
const jwt         	= require('jsonwebtoken');
const jwtSecret 	= process.env.JWT_SECRET;
const jwtExpiry		= process.env.JWT_EXPIRY;
const deviceSecret 	= process.env.DEVICE_SECRET;

// user schema
const userSchema = new mongoose.Schema({
    email: {type: String, required: true},
    names: {type: String, required: true},
    password: {type: String, required: true},
    description: {type: String, required: false},
    avatar: {type: String, required: false},
    role: {type: Number, required: true},
    status: {type: Number, required: true},
    phone: {type: String, required: false}
},{
    timestamps: true,
    autoIndex: false
});

const User = connection.model('User', userSchema);

User.createIndexes();

User.signupUser = async (req, res) => {
    var email   	= req.body.email;
    var names       = req.body.names;
    var password    = bcrypt.hashSync(req.body.password, 10);
    var role      	= 1;
    var status      = 1;
    var query 		= {email, names, password, role, status}

    // create new user
    await new User(query).save().then(user => {
        res.status(200).json({status: "success", message: "Registration successful!", data: user});
    }).catch(err => {
        res.status(500).json({status: "error", message: "Server error!",  err: err});
    });
}

User.authenticate = async (req, res) => {
	// console log value
	var email 		= req.body.email;
	var password 	= req.body.password;

	// fetch data
	await User.findOne({email: email}, function(err, user) {
		if(err) return res.status(500).json({status: "error", message: err});

		// console log value
		if(user == null) return res.status(404).json({status: "error", message: "Invalid mobile!"});
		// console log value

		// compare password
		bcrypt.compare(password, user.password, function(err, isMatch){
			// check password
			if(err){
				throw err; 
			}else if(isMatch === true){
				// sign JWT
				var signature 	= JSON.stringify(user);
				var token 		= jwt.sign({ api: signature }, jwtSecret, {expiresIn: "7d"});

				// exclude password key
				delete user.password;

				// set a global user
				req.session.token 	= token;
				req.session.user 	= user;
				req.session.isLogin = true;

				// console log data
				// console.log(req.session.user);

				// return res.redirect('/dashboard');
				return res.status(200).json({status: "success", message: "Login successul!", data: token});
				// return next();
			}else{
				return res.status(200).json({status: "error", message: "Incorrect password!"});
			}
		});
	});
}

User.updateProfile = async (req, res) => {

}

User.updateAvatar = async (req, res) => {

}

User.viewUserProfile = async (req, res) => {

}

module.exports = User;