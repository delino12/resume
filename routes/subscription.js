var express     = require('express');
var controller  = require('../controllers/subscription');
var router      = express.Router(); 

// Routes related to event
router.get('/', controller.subscribedUsers);
router.post('/subscribe', controller.subscribe);
router.post('/unsubscribe', controller.unsubscribe);

module.exports = router;