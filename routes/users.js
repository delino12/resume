var express 	= require('express');
var controller 	= require('../controllers/users');
var router 		= express.Router();

// Routes related to event
router.get('/:userId', controller.viewUserProfile);
router.post('/signup', controller.signup);
router.put('/avatar', controller.updateAvatar);
router.put('/profile/:userId', controller.updateProfile);

module.exports = router;