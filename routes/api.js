var express     = require('express');
var controller  = require('../controllers/api');
var router      = express.Router(); 

// Routes related to event
router.get('/', controller.index);
router.get('/drafts', controller.fetchUserArticles);
router.get('/articles/:articleId', controller.fetchOneArticle);
router.post('/release/:articleId', controller.releaseArticle);
router.get('/published/articles', controller.getReleasedArticles);
router.get('/count/published', controller.totalPublished);
router.get('/count/drafts', controller.totalDraft);

module.exports = router;