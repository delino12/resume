var express 	= require('express');
var controller 	= require('../controllers/articles');
var middleware  = require('../middlewares/authentication-middleware')
var router 		= express.Router();

// Routes related to event
router.get('/', controller.articles);
router.get('/create', middleware.isLogin, controller.newArticle);
router.post('/save', middleware.isLogin, controller.saveArticle);
router.get('/edit/:articleId', middleware.isLogin, controller.editArticle);
router.get('/drafts', middleware.isLogin, controller.getDraftsArticle);
router.post('/update', middleware.isLogin, controller.updateArticle);
router.get('/:articleId', middleware.isLogin, controller.getOneArticle);
router.post('/delete', middleware.isLogin, controller.deleteOneArticle);

module.exports = router;