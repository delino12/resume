var express 	= require('express');
var controller 	= require('../controllers/index');
var middleware  = require('../middlewares/authentication-middleware')
var router 		= express.Router();

// Routes related to event
router.get('/', controller.index);
router.get('/read/:title/:articleId', controller.article);
router.get('/dashboard', middleware.isLogin, controller.dashboard);
router.get('/.well-known/pki-validation', controller.certum)
router.get('/about', controller.about);
router.get('/privacy', controller.privacy);
router.get('/terms', controller.terms);

module.exports = router;