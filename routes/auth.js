var express 	= require('express');
var controller 	= require('../controllers/auth');
var router 		= express.Router();

// Routes related to event
router.get('/signin', controller.signin);
router.get('/signup', controller.signup);
router.post('/signin', controller.signinUser);
router.post('/signup', controller.signupUser);
router.get('/signout', controller.signout);

module.exports = router;