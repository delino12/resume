require('dotenv').config();
var mail = require("../config/mail");

var notifyNewArticle = async (email, article) => {
	return new Promise((resolve, reject) => {
		var parse_Link = article.title.replace(/[,. ]/g, '-');
		if(process.env('NODE_ENV') == "development"){
			resolve(true);
		}else{
			mail.transport2({
		        from: 'The @code_dreamer <no-reply@ekpotoliberty.com>', // sender address
		        to: email, // list of receivers
		        // bcc: `${emails}`, // list of receivers
		        subject: `${article.title}`, // Subject line
		        html: `
			        <html>
						<head>
							<title>Mail</title>
							<link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">
							<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
							<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
							<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/3.3.7/cerulean/bootstrap.min.css">
							<link href='https://fonts.googleapis.com/css?family=Amiko' rel='stylesheet'>
							<style type="text/css">
								body {
								    font-family: 'Open Sans';font-size: 10px;
								}

								table, td, th {    
								    border: 0px solid #ddd;
								    text-align: left;
								}

								table {
								    border-collapse: collapse;
								    width: 50%;
								}

								th, td {
								    padding: 15px;
								}
							</style>
						</head>
						<body>
							<style type="text/css">
								body {
									font-family: 'Open Sans', sans-serif;
								}
							</style>
							<div style="padding: 0.5rem;margin-left: 10%;margin-right: 10%;margin-to: 5%;text-align:justify;box-shadow: 0px 0px 3px 0px #999;padding: 1rem;">
								Hi there, <br /><br />

								I just published a new article, like I promise you'd be the first to get a mail from me. <br /><br />

								${article.title} <br /><br />

								<img src="${article.featured_image}" class="img-responsive" width="100%" height="425" />

								<br /><br />

			        			To get more details about this article <a href="https://ekpotoliberty.com/read/${parse_Link}/${article._id}">click here to read more...</a>

			        			<br /><br> Thanks, Don't forget to reply, comment or send me your feedback. I will be glad to respond.
			        		</div>
			        	</body>
			        </html>
		        `
		    }).then(val => {
				resolve(val);
			}).catch(error => {
				reject(error);
			});
		}
	});
}

module.exports = {
	notifyNewArticle: notifyNewArticle
}