var Article = require('../models/article');

var articles = async (req, res) => {
	var articles = await Article.find({}).then(articles => articles);
	// console.log(articles);
	res.render('./articles', {articles: articles});
}

var newArticle = (req, res) => {
	var user = req.session.user || {};
	res.render('./create', {user: user})
}

var saveArticle = async (req, res) => {
	// save Article
	await Article.addArticle(req, res);
}

var editArticle = (req, res) => {
	// edit Article
	var articleId = req.params.articleId;
	var user = req.session.user || {};
	res.render('./edit', {articleId: articleId, user: user})
}

var getDraftsArticle = (req, res) => {
	// edit Article
	var user = req.session.user || {};
	res.render('./drafts', {user: user})
}

var updateArticle = async (req, res) => {
	// update Article
	await Article.updateOneArticle(req, res);
}

var getOneArticle = (req, res) => {
	// getOne Article
	var user = req.session.user || {};
	res.render('./view-post', {user: user, articleId: req.params.articleId})
}

var deleteOneArticle = async (req, res) => {
	// deleteOne Article
	await Article.deleteOneArticle(req, res);
}

module.exports = {
	articles: articles,
	newArticle: newArticle,
	saveArticle: saveArticle,
	editArticle: editArticle,
	updateArticle: updateArticle,
	getOneArticle: getOneArticle,
	deleteOneArticle: deleteOneArticle,
	getDraftsArticle: getDraftsArticle
}