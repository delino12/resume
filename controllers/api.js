/**
* Author: Ekpoto Liberty Bernard
* Version: 1.0.2
* Signature: delino12
* Controllers
*/
const Article 		= require('../models/article.js');
const Subscription 	= require('../models/subscriptions.js');

/*
|------------------------------------
| show index page
|------------------------i------------
*/
var index = async (req, res) => {
	res.status(200).json({status: 200, message: "endpoint reached!", data: {}})
}

var saveArticleToDraft = async (req, res) => {
	await Article.addArticle(req, res);
}

var fetchUserArticles = async (req, res) => {
	await Article.getUserArticles(req, res);
}

var fetchOneArticle = async (req, res) => {
	await Article.getOneArticle(req, res);
}

var releaseArticle = async (req, res) => {
	await Article.releaseArticle(req, res);
}

var getReleasedArticles = async (req, res) => {
	await Article.getReleasedArticles(req, res);
}

var totalPublished = async (req, res) => {
	await Article.totalPublished(req, res);
}

var totalDraft = async (req, res) => {
	await Article.totalDraft(req, res);
}

var subscribe = async (req, res) => {
	await Subscription.subscribe(req, res);
}

module.exports = {
	index: index,
	saveArticleToDraft: saveArticleToDraft,	
	fetchUserArticles: fetchUserArticles,
	fetchOneArticle: fetchOneArticle,
	releaseArticle: releaseArticle,
	getReleasedArticles: getReleasedArticles,
	totalPublished: totalPublished,
	totalDraft: totalDraft,
	subscribe: subscribe
}


