/**
* Author: Ekpoto Liberty Bernard
* Version: 1.0.2
* Signature: delino12
* Controllers
*/
const Subscription 	= require('../models/subscriptions.js');

var subscribe = async (req, res) => {
	await Subscription.subscribe(req, res);
}

var unsubscribe = async (req, res) => {
	await Subscription.unsubscribe(req, res);
}

var subscribedUsers = async (req, res) => {
	await Subscription.subscribedUsers(req, res);
}

module.exports = {
	subscribe: subscribe,
	unsubscribe: unsubscribe,
	subscribedUsers: subscribedUsers
}


