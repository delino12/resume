var User = require("../models/users");

var signin = (req, res) => {
	res.render('./signin')
}

var signup = (req, res) => {
	res.render('./signup')
}

var signupUser = async (req, res) => {
	await User.signupUser(req, res);
}

var signinUser = async (req, res) => {
	await User.authenticate(req, res);
}

var signout = (req, res) => {
	// req.session.destroy();
	console.log(req.session);
	req.session.cookie.expires = 1000;
	return res.redirect('/')
}

module.exports = {
	signin: signin,
	signup: signup,
	signupUser: signupUser,
	signinUser: signinUser,
	signout: signout
}