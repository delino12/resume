var Article     = require('../models/article');
var moment      = require('moment');
// var ObjectId    = require('mongodb').ObjectId;

var index = async (req, res) => {
    var article = await Article.findOne({status: 1}).sort({'createdAt': -1}).then(article => article);
    var posted_at = moment(article.createdAt).format('LL');
    var link_url = `${req.protocol}://${req.get('host')}${req.originalUrl}`
    // console.log(req);
    res.render('./articles', {article: article, posted_at: posted_at, link_url: link_url});
}

var article = async (req, res) => {
    var article = await Article.findOne({_id: req.params.articleId, status: 1}).sort({'createdAt': -1}).then(article => article);
    var posted_at = moment(article.createdAt).format('LL');
    var link_url = `${req.protocol}://${req.get('host')}${req.originalUrl}`
    // console.log(link_url);
    res.render('./article', {article: article, posted_at: posted_at, link_url: link_url});
}

var certum = (req, res) => {
	res.writeHead(200, {'Content-Type': 'text/html'});
    res.write(`
    	<html>
    		<head>
    			<title>ekpotoliberty.com</title>
    		</head>
    		<body>
    			e5cba8fbd51559ef9d9d61e88166c2d47f38c0290486972b773ffe0a3edf08e0-certum.pl
    		</body>
    	</html>
    `);
    res.end()
}

var dashboard = (req, res) => {
    var user = req.session.user || {};
    res.render('./dashboard', {user: user});
}

var about = (req, res) => {
    var user = req.session.user || {};
    res.render('./about', {user: user});
}

var terms = (req, res) => {
    res.render('./terms');
}

var privacy = (req, res) => {
    res.render('./privacy');
}

module.exports = {
	index: index,
	certum: certum,
    dashboard: dashboard,
    article: article,
    about: about,
    terms: terms,
    privacy: privacy
}