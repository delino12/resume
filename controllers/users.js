var User = require("../models/users");

// create new user
var signup = async (req, res) => {
	await User.signup(req, res);
}

// update user profile information
var updateProfile = async (req, res) => {
	await User.updateProfile(req, res);
}

// update user avatar
var updateAvatar = async (req, res) => {
	await User.updateAvatar(req, res);
}

// create new user
var viewUserProfile = async (req, res) => {
	await User.viewUserProfile(req, res);
}

module.exports = {
	signup: signup,
	updateProfile: updateProfile,
	updateAvatar: updateAvatar,
	viewUserProfile: viewUserProfile,
}